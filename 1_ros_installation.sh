#!/bin/bash

# ros melodic installation
# It is advisable that you do not change anything in this document and let it run until the end as is

# setup the sources.list
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

# setup the keys
sudo apt install curl -y
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -

# ensure that the system is up-to-date
sudo apt update

# melodic installation
sudo apt install ros-melodic-desktop-full -y

# update the .bashrc
echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
source ~/.bashrc

# installation of dependencies to build ROS packages
sudo apt install python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential -y

# rosdep initialization
sudo apt install python-rosdep
sudo rosdep init
rosdep update
