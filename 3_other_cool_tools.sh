#!/bin/bash
sudo apt update
# Other software installations - feel free to comment those that you do not want to be installed

# install terminator, a cool emulator that provides with functionalitites like: split horizontally/vertically, multigrid-like structure etc.
sudo apt-get install terminator -y

# install vim
sudo apt-get install vim -y

# unity tweak tool - or gnome tweak tool: allows to configure the system the way you want
sudo apt install unity-tweak-tool -y

# snapd - a bundle of app repos that can be installed from the snap store
sudo apt install snapd

# vlc installation
sudo snap install vlc

# gimp for image editing
sudo apt-get install gimp -y

# visual studio code
sudo snap install code --classic

# sublime text
sudo snap install sublime-text --classic

# peek - a screen recording software
sudo add-apt-repository ppa:peek-developers/stable -y
sudo apt update
sudo apt install peek -y

# skype
sudo snap install skype --classic

# telegram desktop
sudo snap install telegram-desktop

# blender - an open source 3D creation software 
sudo snap install blender --classic

# synaptic - graphical package management tool
sudo apt-get install synaptic -y

# android studio
sudo snap install android-studio --classic

# pycharm
sudo snap install pycharm-community --classic

# slack
sudo snap install slack --classic

# spotify
sudo snap install spotify

# discord
sudo snap install discord

# net tools
sudo apt install net-tools


