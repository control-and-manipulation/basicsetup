# Control lab basic setup
> ***We recommend that you use these script files in a clean installation of ubuntu 18.04 LTS (bionic).***

## Contents
The basic installation consists of the following sripts:

- 0_setup.sh: runs the full installation of ROS, UR5, a plethora of tools and the control lab repository. Edit this file if you want to execute specific installation scripts. Run this file after you made sure you are happy with the installation provided/edited.
- 1_ros_installation.sh: basic ROS melodic installation. We do not advise you to edit this file before executing it. 
- 2_UR5_drivers.sh: installation of the UR5 robot and ROS moveit (along with moveit tutorials). Creation of a catkin workspace called ***control_ws*** which will be the working directory for the ROS, UR5, gripper and TII modules. We do not advise you to edit this file before executing it, unless you do not wish to install the moveit tutorials.
- 3_other_cool_tools.sh: a list of a variety of applications (see below). You are free to comment which ever installation you do not find necessary.
- 4_control_repo.sh: installation of the contol lab repository. We do not advise you to edit this file before executing it.


## Installation instructions
Download the files, open a terminal (in ubuntu: Ctrl+Alt+T), go to the folder where the files are located and execute the following command:

```sh
chmod +x 0_setup.sh
```

This will make the 0_setup.sh file executable, and now to run it type:

```sh
./0_setup.sh
```
## Check your system
Gazebo and UR5 are properly installed:
```sh
roslaunch ur_gazebo ur5.launch
```

![UR5 and gazebo example](gazebo_ur5.png)

> :warning: **you can safely ignore the following message for now**: *No p gain specified for pid.  Namespace: /gazebo_ros_control/pid_gains/joint_name*


## To do
Add the control lab repository and installation