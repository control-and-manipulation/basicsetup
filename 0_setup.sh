#!/bin/bash

# change the permissions of the files
chmod +x 1_ros_installation.sh
chmod +x 2_UR5_drivers.sh
chmod +x 3_other_cool_tools.sh
chmod +x 4_control_repo.sh


# execute the files
./1_ros_installation.sh
./2_UR5_drivers.sh
./3_other_cool_tools.sh
./4_control_repo.sh
