#!/bin/bash

# It is advisable that you do not change anything in this document and let it run until the end as is
# setup the ROS environment and Universal Robots ROS driver
source /opt/ros/melodic/setup.bash

# make sure we have the most up to date packages
rosdep update
sudo apt-get update
sudo apt-get dist-upgrade -y

# make sure that the catkin build system is installed
sudo apt-get install ros-melodic-catkin python-catkin-tools

# add other necessary packages
sudo apt-get install ros-melodic-joint-trajectory-controller
sudo apt install ros-$ROS_DISTRO-joint-state-publisher-gui

# install moveit
sudo apt-get install ros-melodic-moveit -y

# start at home directory
eval cd "$HOME"

# create a catkin workspace where we will install all the UR5 software
mkdir -p control_ws/src
cd control_ws/src

# clone the driver
git clone https://github.com/UniversalRobots/Universal_Robots_ROS_Driver.git 
# clone fork of the description. This is currently necessary, until the changes are merged upstream.
git clone -b calibration_devel https://github.com/fmauch/universal_robot.git fmauch_universal_robot

# clone moveit tutorials
sudo apt-get install ros-$ROS_DISTRO-moveit-visual-tools -y
git clone https://github.com/ros-planning/moveit_tutorials.git -b melodic-devel

# go to root
cd ..
# install dependencies
sudo apt update -qq
rosdep update
rosdep install --from-paths src --ignore-src -y

# build the workspace
catkin_make

# activate the workspace (ie: source it)
source devel/setup.bash


# activate the workspace (ie: source it)
source devel/setup.bash

# and then add it to the env variable
echo 'source ~/control_ws/devel/setup.bash' >> ~/.bashrc
